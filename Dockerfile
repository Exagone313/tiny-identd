FROM docker.io/golang:1.22-alpine as builder
WORKDIR /src
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o tiny-identd .

FROM scratch
COPY ./COPYING /COPYING
COPY --from=builder /src/tiny-identd /bin/
ENV PATH=/bin
ENTRYPOINT ["/bin/tiny-identd"]
