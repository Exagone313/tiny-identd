// Copyright (C) 2024 Elouan Martinet <exa@elou.world>
// SPDX-FileCopyrightText: 2024 Elouan Martinet <exa@elou.world>
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"bufio"
	"bytes"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

func main() {
	listen := Getenv("LISTEN", ":8113")
	userId := Getenv("USERID", "nobody")
	log.Println("Listening on", listen)
	ln, err := net.Listen("tcp", listen)
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()

	wg := new(sync.WaitGroup)
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Print(err)
		} else {
			if err := conn.SetDeadline(time.Now().Local().Add(time.Duration(30) * time.Second)); err != nil {
				log.Print("Could not set deadline to ", conn.RemoteAddr(), ": ", err)
				conn.Close()
			} else {
				wg.Add(1)
				log.Println("Handling connection from", conn.RemoteAddr())
				go HandleConnection(userId, wg, conn)
			}
		}
	}
	wg.Wait()
}

func Getenv(name string, defaultValue string) string {
	value := os.Getenv(name)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}

func HandleConnection(userId string, wg *sync.WaitGroup, conn net.Conn) {
	defer wg.Done()
	defer conn.Close()
	reader := bufio.NewReader(&io.LimitedReader{R: conn, N: 64})
	data, err := reader.ReadBytes('\n')
	if err != nil {
		log.Print("Error reading from ", conn.RemoteAddr(), ": ", err)
		return
	}
	trimmedData := bytes.TrimSpace(data)
	writer := bufio.NewWriterSize(conn, 256)
	writer.Write(trimmedData)
	writer.WriteString(":USERID:UNIX:")
	writer.WriteString(userId)
	writer.Write([]byte{'\r', '\n'})
	if err = writer.Flush(); err != nil {
		log.Print("Error writing to ", conn.RemoteAddr(), ": ", err)
		return
	}
	log.Println("Sent response to", conn.RemoteAddr())
}
