# Tiny Identd

Tiny Identification Protocol server ([RFC 1413](https://www.rfc-editor.org/rfc/rfc1413.html)).

Its goal is to return a single user identifier (`USERID`) for any valid query.

## License

See [COPYING](COPYING).
